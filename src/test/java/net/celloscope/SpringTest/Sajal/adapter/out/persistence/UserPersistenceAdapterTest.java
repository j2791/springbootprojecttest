package net.celloscope.SpringTest.Sajal.adapter.out.persistence;

import net.celloscope.SpringTest.Sajal.adapter.out.persistence.entity.UserEntity;
import net.celloscope.SpringTest.Sajal.adapter.out.persistence.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.OPTIONAL;
import static org.mockito.ArgumentMatchers.any;

@DataJpaTest
public class UserPersistenceAdapterTest {

    private UserEntity userEntity;
    private UserRepository userRepository;

    public UserPersistenceAdapterTest(UserEntity userEntity, UserRepository userRepository) {
        this.userEntity = userEntity;
        this.userRepository = userRepository;
    }

    @Test
    void saveDataToTheRepository(){
        //given
        String id = "789";
        String pass = "789";
        String mob = "789";
        //when
        UserEntity getResponse = userRepository.save(new UserEntity(id,pass,mob));
        //then
        assertThat(getResponse).isInstanceOf(UserEntity.class);
    }
}
