package net.celloscope.SpringTest.Sajal.application;

import net.celloscope.SpringTest.Sajal.adapter.out.persistence.entity.UserEntity;
import net.celloscope.SpringTest.Sajal.adapter.out.persistence.repository.UserRepository;
import net.celloscope.SpringTest.Sajal.domain.User;
import org.assertj.core.internal.bytebuddy.utility.dispatcher.JavaDispatcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class UserRegistrationServiceTest {

    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserRegistrationService userRegistrationService;


    @Test
    void givenNullUserIdPassMobile_whenRegisterUserIsInvoked_shouldThrowException() {
        assertThrows(RuntimeException.class, () -> userRegistrationService.registerUser(null, null, null));
    }

    @Test
    void givenUserIdPassMobile_whenRegisterUserIsInvoked_shouldReturnSuccessfulResponseMessage() {
        //given
        String id = "789";
        String pass = "789";
        String mob = "789";
        String expectedMessage = "Successful";
        //when
//        Mockito.when(userRepository.save(new UserEntity(anyString(),anyString(),anyString()))).thenReturn(new UserEntity(id,pass,mob));
        Mockito.when(userRepository.save(any(UserEntity.class))).thenReturn(new UserEntity());
        String response = userRegistrationService.registerUser(id,pass,mob);
        //then
        assertThat(response).isEqualTo(expectedMessage);
    }


}
