package net.celloscope.SpringTest.Sajal.application;

import net.celloscope.SpringTest.Sajal.application.in.UserFunctionalityUseCase;
import net.celloscope.SpringTest.Sajal.application.out.UserDataPersistencePort;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationService implements UserFunctionalityUseCase {

    private UserDataPersistencePort userDataPersistencePort;

    public UserRegistrationService(UserDataPersistencePort userDataPersistencePort) {
        this.userDataPersistencePort = userDataPersistencePort;
    }

    @Override
    public String userLogin(String userId, String pass) {
        return userDataPersistencePort.userLogin(userId, pass);
    }

    @Override
    public String registerUser(String userId, String pass, String mobileNum) throws RuntimeException{
        return userDataPersistencePort.registerUser(userId, pass, mobileNum);
    }

    @Override
    public String updatePassword(String userId, String pass) {
        return userDataPersistencePort.updatePassword(userId, pass);
    }
}
