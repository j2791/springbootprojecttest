package net.celloscope.SpringTest.Sajal.application.in;

public interface UserFunctionalityUseCase {
    String userLogin(String userId, String pass);
    String registerUser(String userId, String pass, String mobileNum);
    String updatePassword(String userId, String pass);
}
