package net.celloscope.SpringTest.Sajal.application.out;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.celloscope.SpringTest.Sajal.domain.User;
import org.springframework.stereotype.Component;

@Component
public interface UserDataPersistencePort {
    User userLogin(String userId, String pass);
    User registerUser(String userId, String pass, String mobileNum);
    User updatePassword(String userId, String pass);
}
