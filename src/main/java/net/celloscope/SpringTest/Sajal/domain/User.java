package net.celloscope.SpringTest.Sajal.domain;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Setter
@Getter
public class User {
    String userId;
    String pass;
    String mobileNum;
}
