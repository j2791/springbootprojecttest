package net.celloscope.SpringTest.Sajal.adapter.in.controller;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.SpringTest.Sajal.application.in.UserFunctionalityUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class RegistrationController {
    private UserFunctionalityUseCase userFunctionalityUseCase;

    public RegistrationController(UserFunctionalityUseCase userFunctionalityUseCase) {
        this.userFunctionalityUseCase = userFunctionalityUseCase;
    }


    // curl http://localhost:8080/register/2/12/123

    @RequestMapping(method = RequestMethod.GET, value = "/register/{userId}/{pass}/{mobileNum}")
    @ResponseBody
    public ResponseEntity<String> registerUser(
            @PathVariable(required = true) String userId,
            @PathVariable(required = true) String pass,
            @PathVariable(required = true) String mobileNum
    ) {
        String registerUserMessage = userFunctionalityUseCase.registerUser(userId, pass, mobileNum);
        if (!registerUserMessage.isEmpty())
            return ResponseEntity.ok("Registration Complete!");
        return ResponseEntity.badRequest().body("User Already Exists!");
    }

    //curl http://localhost:8080/login/2/12

    @RequestMapping(method = RequestMethod.GET, value = "/login/{userId}/{pass}")
    @ResponseBody
    public ResponseEntity<String> loginUser(
            @PathVariable(required = true) String userId,
            @PathVariable(required = true) String pass
    ) {
        String loginUserMessage = userFunctionalityUseCase.userLogin(userId, pass);
        if (!loginUserMessage.isEmpty())
            return ResponseEntity.ok("Successfully LoggedIn!");
        return ResponseEntity.badRequest().body("Invalid User / Password!");
    }


    //curl http://localhost:8080/pass-change/2/12

    @RequestMapping(method = RequestMethod.GET, value = "/pass-change/{userId}/{pass}")
    @ResponseBody
    public ResponseEntity<?> updatePassword(
            @PathVariable(required = true) String userId,
            @PathVariable(required = true) String pass
    ) {
        String updateUserPassword = userFunctionalityUseCase.updatePassword(userId, pass);
        if (!updateUserPassword.isEmpty())
            return ResponseEntity.ok("Password has been Changed!");
        return ResponseEntity.badRequest().body("Invalid User for updating password");
    }


}
