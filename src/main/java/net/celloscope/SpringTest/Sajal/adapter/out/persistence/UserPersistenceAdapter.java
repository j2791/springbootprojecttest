package net.celloscope.SpringTest.Sajal.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.celloscope.SpringTest.Sajal.adapter.out.persistence.entity.UserEntity;
import net.celloscope.SpringTest.Sajal.adapter.out.persistence.repository.UserRepository;
import net.celloscope.SpringTest.Sajal.application.out.UserDataPersistencePort;
import net.celloscope.SpringTest.Sajal.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Optional;

@Component
@Slf4j
public class UserPersistenceAdapter implements UserDataPersistencePort {

    private UserEntity userEntity;
    private final UserRepository userRepository;

    public UserPersistenceAdapter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String userLogin(String userId, String pass) {

        Optional<UserEntity> tryLoginWithUserId = userRepository.findById(userId);
        if (tryLoginWithUserId.isEmpty())
            return "";

        String requestPass = tryLoginWithUserId.get().getPass();

        Base64.Decoder decoder = Base64.getDecoder();
        byte[] bytes = decoder.decode(requestPass);
        requestPass = new String(bytes);

        log.info("Dencoded Pass : {}", requestPass);

        log.info("requestPass = {} | pass = {}", requestPass, pass);
        if (!requestPass.equals(pass))
            return "";
        return "Successful";

    }

    @Override
    public String registerUser(String userId, String pass, String mobileNum) {

        Optional<UserEntity> tryRegisterWithUserId = userRepository.findById(userId);
        log.info("If userID isPresent = {}", tryRegisterWithUserId.isPresent());

        if (tryRegisterWithUserId.isPresent()) {
            return "";
        }
        log.info("id = {} pass = {} mobileNum = {}", userId, pass, mobileNum);

        Base64.Encoder encoder = Base64.getEncoder();
        String originalString = pass;
        String encodedString = encoder.encodeToString(originalString.getBytes());
        log.info("Encoded Pass : {}", encodedString);


        userRepository.save(new UserEntity(userId, encodedString, mobileNum));
        return "Successful";

    }

    @Override
    public String updatePassword(String userId, String pass) {

        Optional<UserEntity> tryRegisterWithUserId = userRepository.findById(userId);
        if (tryRegisterWithUserId.isPresent()) {
            String mobileNum = tryRegisterWithUserId.get().getMobileNum();
            UserEntity updateUser = new UserEntity(userId, pass, mobileNum);
            userRepository.save(updateUser);
            return "Success";
        }
        return "";

    }
}
