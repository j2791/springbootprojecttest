package net.celloscope.SpringTest.Sajal.adapter.out.persistence.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="UserDB")
public class UserEntity {
    @Id
    @NotNull
    @Column(name = "UserID")
    private String userId;

    @NotNull
    @Column(name = "Password")
    private String pass;

    @NotNull
    @Column(name = "MobileNumber")
    private String mobileNum;

}
