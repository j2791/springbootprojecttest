package net.celloscope.SpringTest.Sajal.adapter.out.persistence.repository;

import net.celloscope.SpringTest.Sajal.adapter.out.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity,String> {
}
